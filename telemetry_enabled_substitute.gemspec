# -*- encoding: utf-8 -*-
Gem::Specification.new do |s|
  s.name = "telemetry_enabled_substitute"
  s.version = "0.0.0"
  s.summary = "Code used at the Utah Microservices Meetup in March 2023"
  s.description = "Code used at the Utah Microservices Meetup in March 2023"

  s.authors = ["ethan@suchsoftware.com"]
  s.homepage = "https://practicalmicroservices.com"
  s.licenses = ["MIT"]

  s.require_paths = ["lib"]
  s.files = Dir.glob("{lib}/**/*")
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = ">= 2.4"

  s.add_runtime_dependency "eventide-postgres"
  s.add_runtime_dependency "evt-telemetry"
  s.add_runtime_dependency "evt-virtual"

  s.add_development_dependency "test_bench"
end
