module Text
  module Write
    def self.included(cls)
      cls.class_exec do
        include Dependency
        include Virtual

        dependency :store
        dependency :telemetry, ::Telemetry

        extend Build
        extend Call
        extend Configure

        abstract :configure

        const_set :Substitute, Substitute
      end
    end

    module Build
      def build(file_name)
        new.tap do |instance|
          ::Telemetry.configure instance
          instance.configure(file_name)
        end
      end
    end

    module Call
      def call(text, file_name)
        instance = build(file_name)
        instance.(text)
      end
    end

    module Configure
      def configure(receiver, file_name)
        instance = build(file_name)
        receiver.write = instance
      end
    end


    def call(text)
      store.(text)
      telemetry.record(:written, text)
    end
    alias :write :call

    def self.register_telemetry_sink(writer)
      sink = Telemetry.sink
      writer.telemetry.register sink
      sink
    end

    module Telemetry
      class Sink
        include ::Telemetry::Sink

        record :written
      end

      def self.sink
        Sink.new
      end
    end
  end
end
