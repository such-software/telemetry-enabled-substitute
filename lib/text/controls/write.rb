module Text
  module Controls
    module Write
      def self.example
        file_name = FileName.example

        Example.build(file_name)
      end

      class Example
        include ::Text::Write

        virtual :configure
      end
    end
  end
end
