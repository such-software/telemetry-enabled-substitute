module Text
  module File
    class Write
      include Text::Write

      def configure(file_name)
        Storage::File::Store.configure(self, file_name)
      end
    end
  end
end
