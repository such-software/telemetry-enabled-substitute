module Text
  module Write
    module Substitute
      def self.build
        Substitute::Write.build.tap do |substitute_writer|
          sink = Text::Write.register_telemetry_sink(substitute_writer)
          substitute_writer.sink = sink
        end
      end

      class Write
        include Text::Write

        attr_accessor :sink

        def self.build
          new.tap do |instance|
            ::Telemetry.configure instance
          end
        end

        def writes(&blk)
          return sink.written_records if blk.nil?

          sink.written_records.select do |record|
            blk.call(record.data)
          end
        end
      end
    end
  end
end
