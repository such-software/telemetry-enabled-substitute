module Storage
  module File
    class Store
      def self.call(text, file_name)
        instance = build(file_name)
        instance.(text)
      end

      def self.build(file_name)
        new(file_name)
      end

      def self.configure(receiver, file_name)
        instance = build(file_name)

        receiver.store = instance
      end

      def initialize(file_name)
        @file_name = file_name
      end

      def call(text)
        ::File.open(@file_name, "w+") do |file|
          file.puts text
        end
      end
    end
  end
end
