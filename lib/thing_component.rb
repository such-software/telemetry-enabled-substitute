require "eventide/postgres"

require "thing_component/messages/commands/do"

require "thing_component/thing"
require "thing_component/projection"
require "thing_component/store"

require "thing_component/handlers/commands"

require "thing_component/consumers/commands"

require "thing_component/start"
