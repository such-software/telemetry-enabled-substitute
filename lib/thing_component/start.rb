module ThingComponent
  module Start
    def self.call
      Consumers::Commands.start("thing:command")
    end
  end
end
