# Handler user guide: http://docs.eventide-project.org/user-guide/handlers.html
# Message user guide: http://docs.eventide-project.org/user-guide/messages-and-message-data/

module ThingComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messages::Commands

      dependency :write, Text::File::Write

      def configure
        Text::File::Write.configure(self, "tmp/test-component.txt")
      end

      handle Do do |do_command|
        thing_id = do_command.thing_id

        write.(thing_id)
      end
    end
  end
end
