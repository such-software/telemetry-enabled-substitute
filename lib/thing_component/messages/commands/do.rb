module ThingComponent
  module Messages
    module Commands
      class Do
        include Messaging::Message

        attribute :thing_id, String
      end
    end
  end
end
