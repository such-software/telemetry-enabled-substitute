module ThingComponent
  module Controls
    module Commands
      module Do
        def self.example(thing_id: ID.example)
          Messages::Commands::Do.build(thing_id:)
        end
      end
    end
  end
end
