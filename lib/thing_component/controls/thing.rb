module ThingComponent
  module Controls
    module Thing
      def self.example
        thing = ThingComponent::Thing.build

        thing.id = self.id
        thing.something_happened_time = Time::Effective::Raw.example

        thing
      end

      def self.id
        ID.example(increment: id_increment)
      end

      def self.id_increment
        1111
      end

      module New
        def self.example
          ThingComponent::Thing.build
        end
      end

      module Identified
        def self.example
          thing = New.example
          thing.id = Thing.id
          thing
        end
      end
    end
  end
end
