require_relative "../../automated_init"

context "Storage" do
  context "File" do
    context "Store" do
      text = Storage::Controls::Text.example
      file_name = Storage::Controls::FileName.example

      File.open(file_name, "w") {|file| file.truncate(0) }

      Storage::File::Store.(text, file_name)

      lines = File.readlines(file_name, chomp: true)

      test "Text was written to the file" do
        assert(lines.length == 1)
      end

      test "Written text matches" do
        line = lines.first

        assert(line == text)
      end
    end
  end
end
