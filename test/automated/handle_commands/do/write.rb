require_relative "../../automated_init"

context "Handle Commands" do
  context "Do" do
    context "Write" do
      handle = ThingComponent::Handlers::Commands.new

      do_command = ThingComponent::Controls::Commands::Do.example 
      thing_id = do_command.thing_id

      handle.(do_command)

      writer = handle.write

      text = writer.writes do |txt|
        txt == thing_id
      end

      test "ID was written" do
        refute(text.nil?)
      end
    end
  end
end
