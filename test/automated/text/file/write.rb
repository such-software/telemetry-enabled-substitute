require_relative "../../automated_init"

context "File" do
  context "Write" do
    text = Controls::Text.example
    file_name = Controls::FileName.example

    File.open(file_name, "w") {|file| file.truncate(0) }

    ::Text::File::Write.(text, file_name)

    lines = File.readlines(file_name, chomp: true)

    test "Text was written to file" do
      assert(lines.length == 1)
    end

    test "Written text matches" do
      line = lines.first

      assert(line == text)
    end
  end
end

