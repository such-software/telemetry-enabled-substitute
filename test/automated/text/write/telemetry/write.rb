require_relative '../../../automated_init'

context "Write" do
  context "Telemetry" do
    context "Write" do
      text = Controls::Text.example

      writer = Controls::Write.example

      sink = Write.register_telemetry_sink(writer)

      writer.write(text)

      test "Records written telemetry" do
        assert(sink.recorded_written?)
      end

      context "Recorded Data" do
        data = sink.records[0].data

        test "text" do
          assert(data == text)
        end
      end
    end
  end
end

