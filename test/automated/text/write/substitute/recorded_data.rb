require_relative '../../../automated_init'

context "Write" do
  context "Substitute" do
    text = Controls::Text.example
    file_name = Controls::FileName.example
    writer = Write::Substitute.build(file_name)

    writer.(text)

    context "Recorded Data" do
      test "No block arguments" do
        assert(writer.writes.length == 1)
      end

      test "Text block argument" do
        assert(writer.writes { |txt| txt == text }.length == 1 )
      end
    end
  end
end

