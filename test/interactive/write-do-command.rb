require_relative "./interactive_init"

id = Identifier::UUID::Random.get
command = ThingComponent::Messages::Commands::Do.build
command.thing_id = id

Messaging::Postgres::Write.(command, "thing:command")


